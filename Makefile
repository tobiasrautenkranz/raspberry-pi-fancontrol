LDFLAGS := -lwiringPi
CXXFLAGS := -std=c++14 -Wall

fancontrol: fancontrol.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) $^ -o $@

.PHONY: clean
clean:
	rm -f -- fancontrol

.PHONY: install
install: fancontrol
	install -o root -g root fancontrol /usr/local/sbin/
	install -o root -g root fancontrol.service /etc/systemd/system/
	@echo "run:\n systemctl deamon-reload\n systemctl enable fancontrol.service\nto execute on startup"
